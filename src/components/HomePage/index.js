import React from 'react'
import { db } from '../../configs/firebase.config'
import { getAuth } from 'firebase/auth'
import { getDoc, doc } from 'firebase/firestore'
import { useState, useEffect } from 'react'
import { async } from '@firebase/util'
import { Link } from 'react-router-dom'
import Logo from "../../rps.webp"

export default function Home() {
    const [data, setData] = useState(null)
    const auth = getAuth()

    const getData = async () => {
        const docRef = doc(db, "users", auth?.currentUser?.uid)
        try {
            const result = await getDoc(docRef)
            if (result.exists()) {
                setData(result?.data())
            } else {
                setData(null)
            }
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
            <div className="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
                <div className="md:flex">
                    <div className="md:shrink-0">
                        <img src={Logo} className="h-48 w-full object-cover md:h-full md:w-48" alt="Man looking at item at a store" />
                    </div>
                    <div className="p-8">
                        <div className="uppercase tracking-wide text-sm text-indigo-500 font-semibold">Rock Paper Scissors</div>
                        <a className="block mt-1 text-lg leading-tight font-medium text-black hover:underline"><Link to="/Game"> Click Here To Play</Link></a>
                        <p className="mt-2 text-slate-500">Rock Paper Scissors is an old Game, You will play Against Computer. Enjoyy the Games!</p>
                    </div>
                </div>

            </div>
            <br></br>
            <div className="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-2xl">
                <div className="md:flex">
                    <div className="md:shrink-0">
                        <img className="h-48 w-full object-cover md:h-full md:w-48" alt="Man looking at item at a store" />
                    </div>
                    <div className="p-8">
                        <div className="uppercase tracking-wide text-sm text-indigo-500 font-semibold">Another Game</div>
                        <a className="block mt-1 text-lg leading-tight font-medium text-black hover:underline">Another Game</a>
                        <p className="mt-2 text-slate-500">Another Game Explanations Another Game Explanations Another Game Explanations Another Game Explanations</p>
                    </div>
                </div>

            </div>
        </>
    )
}
